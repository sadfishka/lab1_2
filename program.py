import psycopg2
from pandas import DataFrame

conn = psycopg2.connect(dbname='test', user='postgres', password='123456', host='localhost')
cursor = conn.cursor()
cursor.execute('SELECT full_name, birth_date FROM group_list')
df = DataFrame(cursor.fetchall())
res = df.sort_values(by='date_of_birth')
res_young = f"Самый старший студент:\n{res['name'].values[0]}\t{res['date_of_birth'].values[0]}"
res_old = f"Самый младший студент:\n{res['name'].values[res['name'].size - 1]}" \
        f"\t{res['date_of_birth'].values[res['date_of_birth'].size - 1]}"
print(res_young + '\n\n' + res_old)
with open('./output/result.txt', "w") as file:
    file.write(res_young + '\n\n' + res_old)

