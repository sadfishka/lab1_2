FROM python:latest

SHELL ["/bin/bash", "-c"]
WORKDIR ./app
RUN pip install virtualenv
RUN virtualenv venv
RUN source venv/bin/activate
RUN pip install openpyxl
RUN pip install setuptools
RUN pip install pandas
RUN pip install psycopg2
RUN pip install sqlalchemy

COPY init_data.py ./
COPY group_list.xlsx ./

CMD [ "python", "data_service.py"]
CMD [ "python", "program.py"]

