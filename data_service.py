import pandas as pd
from sqlalchemy import create_engine

engine = create_engine('postgresql://postgres:123456@localhost:5432/test')
file = 'data.xlsx'

xl = pd.ExcelFile(file)
df = xl.parse('group')
df.to_sql('group', engine)
